def measure(n = 1)

  starting_time = Time.now

  n.times { yield }

  ending_time = Time.now
  elapsed_time = ending_time - starting_time
  elapsed_time / n

end
